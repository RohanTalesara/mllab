from operator import itemgetter
import random
import math
import pandas as pd
import numpy as np

# GLOBAL VARIABLES

solution_found = False
popN = 10 #n number of chromos per population
genesPerCh = 35
max_iterations = 100

#target number. Our Algorithm will try to find an expression to generate this number
dataset = pd.read_csv('6.csv')
dataset = dataset.drop(dataset.columns[37], axis=1)
dataset = dataset.drop(dataset.columns[36], axis=1)
dataset = dataset.drop(dataset.columns[0], axis=1)


crossover_rate = 0.7
mutation_rate = 0.05

"""Generates random population of chromos"""
def generatePop ():
  chromos, chromo = [], []
  for eachChromo in range(popN):
    chromo = []
    for bit in range(genesPerCh):
      chromo.append(random.randint(0,1))
    chromos.append(chromo)
  return chromos

"""Takes a binary list (chromo) and returns a protein (mathematical expression in string)"""
def translate (chromo):
  protein, chromo_string = '',''
  need_int = True
  a, b = 0, 4 # ie from point a to point b (start to stop point in string)
  for bit in chromo:
    chromo_string += str(bit)
  for gene in range(genesPerCh):
    if chromo_string[a:b] == '1111' or chromo_string[a:b] == '1110': 
      continue
    elif chromo_string[a:b] != '1010' and chromo_string[a:b] != '1011' and chromo_string[a:b] != '1100' and chromo_string[a:b] != '1101':
      if need_int == True:
        protein += genetic_code[chromo_string[a:b]]
        need_int = False
        a += 4
        b += 4
        continue
      else:
        a += 4
        b += 4
        continue
    else:
      if need_int == False:
        protein += genetic_code[chromo_string[a:b]]
        need_int = True
        a += 4
        b += 4
        continue
      else:
        a += 4
        b += 4
        continue
  if len(protein) %2 == 0:
    protein = protein[:-1]
  return protein
  
#Evaluation is left to right. No precedence is observed.
#Two operands and an operator are combined.
# Ex: 2+5+7*20/3 will be 7+7*20/3 which will be 14*20/3 = 280/3 = 93.33333
"""Evaluates the mathematical expressions in number + operator blocks of two"""
def evaluate(protein):
  a = 3
  b = 5
  output = -1
  lenprotein = len(protein) 
  if lenprotein == 0:
    output = 0
  if lenprotein == 1:
    output = int(protein)
  if lenprotein >= 3:
    try :
      output = eval(protein[0:3])
    except ZeroDivisionError:
      output = 0
    if lenprotein > 4:
      while b != lenprotein+2:
        try :
          output = eval(str(output)+protein[a:b])
        except ZeroDivisionError:
          output = 0
        a+=2
        b+=2  
  return output

"""Calulates fitness as a fraction of the total fitness"""

def displayFit (error):
  bestFitDisplay = 100
  dashesN = int(error * bestFitDisplay)
  dashes = ''
  for j in range(bestFitDisplay-dashesN):
    dashes+=' '
  for i in range(dashesN):
    dashes+='+'
  return dashes


def calcfitness(chromo):
    final = 0.0
    for index, row in dataset.iterrows():
        match = 0
        for i in range(35):
            if(chromo[i] == row[i]):
                match+=1
        final += match/35
    final = final/120*100
    return final

"""Takes a population of chromosomes and returns a list of tuples where each chromo is paired to its fitness scores and ranked according to its fitness"""
def rankPop (chromos):
  proteins, outputs, errors = [], [], []
  i = 1
  rank = []
  # translate each chromo into mathematical expression (protein), evaluate the output of the expression,
  # calculate the inverse error of the output 
  for chromo in chromos: 
    fitness = calcfitness(chromo)
    rank.append([chromo,fitness])
  rank.sort(key=lambda x: x[1],reverse=True)
  rankedPop=[]
  for i in rank:
    i[0].append(i[1])
    rankedPop.append(i[0])
  return rankedPop

""" taking a ranked population selects two of the fittest members using roulette method"""
def selectFittest (fitnessScores, rankedChromos):
  while 1 == 1: # ensure that the chromosomes selected for breeding are have different indexes in the population
    index1 = roulette (fitnessScores)
    index2 = roulette (fitnessScores)  
    if index1 == index2:
      continue
    else:
      break

  ch1 = rankedChromos[index1] # select  and return chromosomes for breeding 
  ch2 = rankedChromos[index2]
  return ch1, ch2

"""Fitness scores are fractions, their sum = 1. Fitter chromosomes have a larger fraction.  """
def roulette (fitnessScores):
  cumalativeFitness = 0.0
  r = random.randint(0,500)
  
  for i in range(len(fitnessScores)): 
    cumalativeFitness += fitnessScores[i] 

    if cumalativeFitness > r: 
      return i

#single point crossover
def crossover (ch1, ch2):
  # at a random chiasma
  r = random.randint(0,genesPerCh)
  return ch1[:r]+ch2[r:], ch2[:r]+ch1[r:]

#Mutation. Probabilistic
def mutate (ch):
  mutatedCh = []
  for i in ch:
    if random.random() < mutation_rate:
      if i == 1:
        mutatedCh.append(0)
      else:
        mutatedCh.append(1)
    else:
      mutatedCh.append(i)
  #assert mutatedCh != ch
  return mutatedCh
      
"""Using breed and mutate it generates two new chromos from the selected pair"""
def breed (ch1, ch2):
  
  newCh1, newCh2 = [], []
  if random.random() < crossover_rate: # rate dependent crossover of selected chromosomes
    newCh1, newCh2 = crossover(ch1, ch2)
  else:
    newCh1, newCh2 = ch1, ch2
  newnewCh1 = mutate (newCh1) # mutate crossovered chromos
  newnewCh2 = mutate (newCh2)
  
  return newnewCh1, newnewCh2

""" Taking a ranked population return a new population by breeding the ranked one"""
def iteratePop (rankedPop):
  fitnessScores = [ item[-1] for item in rankedPop ] # extract fitness scores from ranked population
  rankedChromos = [ item[0:35] for item in rankedPop ] # extract chromosomes from ranked population
  #print(rankedChromos)
  newpop = []
  newpop.extend(rankedChromos[:6]) # known as elitism, conserve the best solutions to new population
  
  while len(newpop) != popN:
    ch1, ch2 = [], []
    ch1, ch2 = selectFittest (fitnessScores, rankedChromos) # select two of the fittest chromos
     
    ch1, ch2 = breed (ch1, ch2) # breed them to create two new chromosomes 
    newpop.append(ch1) # and append to new population
    newpop.append(ch2)
  
  return newpop
  
def main(): 
  #configureSettings ()
  chromos = generatePop() #generate new population of random chromosomes
  #print(chromos)
  iterations = 0
  global solution_found
  while iterations != max_iterations and solution_found != True:
    # take the pop of random chromos and rank them based on their fitness score/proximity to target output
    rankedPop = rankPop(chromos) 
    
    print ('\nCurrent iterations:', iterations)
    
    if solution_found != True:
      # if solution is not found iterate a new population from previous ranked population
      chromos = []
      chromos = iteratePop(rankedPop)
      print("hi")      
      iterations += 1
    else:
      break
  for i in chromos:
     print(i)
  test1 = [0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0,0,0,0,0,1,1]
  test2 = []

      
if __name__ == "__main__":
    main()

