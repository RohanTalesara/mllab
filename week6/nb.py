import os
import re
import string
import math

#defining the data directory name 
DATA_DIR = 'enron'
#target labels
target_names = ['ham', 'spam']

def get_data(DATA_DIR):
	'''
	Function to read all files in the subfolders in DATA_DIR folder.
	Folders with 'spam' in the end of their names are training samples labelled as spam.
	The ones with 'ham' in the end of their names are those labelled as not spam.
	returns 2 lists: data and target
	target stores the target labels of corresponding data in data list.
	'''

	#get all subfolders in DATA_DIR
	subfolders = ['enron%d' % i for i in range(1,7)]
 
	data = []
	target = []
	for subfolder in subfolders:
		# spam
		#List all directories with spam in the end of their names
		spam_files = os.listdir(os.path.join(DATA_DIR, subfolder, 'spam'))
		#Loop through all the spam files
		for spam_file in spam_files:
			with open(os.path.join(DATA_DIR, subfolder, 'spam', spam_file), encoding="latin-1") as f:
				#append the file to data
				data.append(f.read())
				#set corresponding target to 1
				target.append(1)

		# ham
		#List all directories with ham in the end of their names
		ham_files = os.listdir(os.path.join(DATA_DIR, subfolder, 'ham'))
		#loop through all the ham files
		for ham_file in ham_files:
			with open(os.path.join(DATA_DIR, subfolder, 'ham', ham_file), encoding="latin-1") as f:
				#append the file to data
				data.append(f.read())
				#set corresponding target to 0
				target.append(0)

	return data, target

class SpamDetector(object):
	"""Implementation of Naive Bayes for binary classification"""
	def clean(self, s):
		'''
		Returns s with all punctuation marks removed.
		'''
		#translator is a dictionary mapping of which cahracters to remove from s.
		#every punctuation mark is mapped to None, meaning it must be removed
		translator = str.maketrans("", "", string.punctuation)

		#uses the transator dictionary generated before and maps characters of s accordingly
		#basically, in our case it removes all the punctuation marks
		return s.translate(translator)

	def tokenize(self, text):
		'''
		cleans the text using clean() fucntion, makes all characters lower case
		and splits all words in this resulting text into single words put into a list.
		Returns this list.
		'''

		#clean the text and make it lower case
		text = self.clean(text).lower()
		#split the cleaned text into individual words
		return re.split("\W+", text)

	def get_word_counts(self, words):
		'''
		Takes a list of words as input and returns a dictionary with the frequency of each word.
		'''
		word_counts = {}
		for word in words:
			word_counts[word] = word_counts.get(word, 0.0) + 1.0
		return word_counts

	def fit(self, X, Y):
		#dictionary to store number of messages of type spam and ham
		self.num_messages = {}
		self.log_class_priors = {}
		self.word_counts = {}
		self.vocab = set()

		#total number of messages
		n = len(X)
		#setting number of spam messages as number of samples with label 1
		self.num_messages['spam'] = sum(1 for label in Y if label == 1)
		#setting number of ham messages as number of samples with label 0
		self.num_messages['ham'] = sum(1 for label in Y if label == 0)
		#prior probability of spam class
		self.log_class_priors['spam'] = math.log(self.num_messages['spam'] / n)
		#prior probability of ham class
		self.log_class_priors['ham'] = math.log(self.num_messages['ham'] / n)

		#word_counts['spam'] is a dictionary of word counts of words
		#appearing in samples labelled as spam
		self.word_counts['spam'] = {}
		#word_counts['ham'] is a dictionary of word counts of words
		#appearing in samples labelled as ham
		self.word_counts['ham'] = {}

		#go through all train samples
		for x, y in zip(X, Y):
			#get the label of the sample
			c = 'spam' if y == 1 else 'ham'
			#find frequency of each word in the sample
			counts = self.get_word_counts(self.tokenize(x))

			#for each word in the sample
			for word, count in counts.items():
				#add the word to the vocabulary
				if word not in self.vocab:
					self.vocab.add(word)
				
				#update the word count of the word in the word_counts['spam']
				#or word_counts['ham'] dictionary based on c
				if word not in self.word_counts[c]:
					self.word_counts[c][word] = 0.0

				self.word_counts[c][word] += count

	def predict(self, X):
		'''
		give prediction for test set X.
		'''
		#list to store predicted labels
		result = []

		#go through all test samples
		for x in X:
			#clean, tokenize and get word frequency for the test sample
			counts = self.get_word_counts(self.tokenize(x))
			#initialize the spam and ham score to 0
			spam_score = 0
			ham_score = 0

			#find posterior probability for each word in the test case and use it to find the spam and ham scores
			for word, _ in counts.items():
				#if word is not in vocabulary, skip it
				if word not in self.vocab: continue

				# add Laplace smoothing
				log_w_given_spam =( (self.word_counts['spam'].get(word, 1.0) + 1) / (self.num_messages['spam'] + len(self.vocab)) )
				log_w_given_ham =( (self.word_counts['ham'].get(word, 1.0) + 1) / (self.num_messages['ham'] + len(self.vocab)) )

				#add probability of word being spam
				spam_score += log_w_given_spam
				#add probability of word being ham
				ham_score += log_w_given_ham

			#multiply with prior probability of spam
			spam_score *= self.log_class_priors['spam']
			#multiply with prior probability of ham
			ham_score *= self.log_class_priors['ham']

			#label depends on which score is higher
			if spam_score > ham_score:
				result.append(1)
			else:
				result.append(0)
		return result

if __name__ == '__main__':
	#get the data (X) and corresponding target values (y)
	X, y = get_data(DATA_DIR)
	#make a spamDetector object
	MNB = SpamDetector()
	#Train the classifier on all data above index 100
	MNB.fit(X[100:], y[100:])

		#use data from index 0-99 as test data
	pred = MNB.predict(X[:100])
	#get the actual labels for the test set
	true = y[:100]

		#find accuracy by comparing the predicted values with actual values
		#accuracy = (number of correct predictions)/(total numbe of test samples)
	accuracy = sum(1 for i in range(len(pred)) if pred[i] == true[i]) / float(len(pred))
	print("{0:.4f}".format(accuracy))
