import os
import re
import string
import math
 
DATA_DIR = 'enron'
target_names = ['ham', 'spam']


# Get data function for getting the input data which is spam and ham folder
# Input paramter : Directory in whihc the we have two folder spam and ham
def get_data(DATA_DIR):
    # Getting the name of the subfolder
    # enron1, enron2
    subfolders = ['enron%d' % i for i in range(1,7)]
 
    # Input data list
    data = []
    # target list
    # For spam the target value is taken as 1
    # For non spam the target value is taken as 0
    target = []
    for subfolder in subfolders:
		# spam
        # Getting all the spam files names
        spam_files = os.listdir(os.path.join(DATA_DIR, subfolder, 'spam'))
        # Iterating over each file and reading appending the content of the files to data list
        # And appending the corresponding target value for it, because here we are here in the spam folder we append the 
        # target value as 1
        for spam_file in spam_files:
            with open(os.path.join(DATA_DIR, subfolder, 'spam', spam_file), encoding="latin-1") as f:
                data.append(f.read())
                target.append(1)
 
		# ham
        # Iterating over each file and reading appending the content of the files to data list
        # And appending the corresponding target value for it, because here we are here in the ham folder we append the 
        # target value as 0
        ham_files = os.listdir(os.path.join(DATA_DIR, subfolder, 'ham'))
        for ham_file in ham_files:
            with open(os.path.join(DATA_DIR, subfolder, 'ham', ham_file), encoding="latin-1") as f:
                data.append(f.read())
                target.append(0)
    
    return data, target

class SpamDetector(object):
    """Implementation of Naive Bayes for binary classification"""
    
    # This function cleans the data set , by removing the punctuation comma etc. and return the cleaned data
    def clean(self, s):
        translator = str.maketrans("", "", string.punctuation)
        return s.translate(translator)
    

    # Splitting of the data into words and returing the tokenise list
    def tokenize(self, text):
        text = self.clean(text).lower()
        return re.split("\W+", text)
 
    # Getting the word count of each input sentecnce return the word count of each input data
    def get_word_counts(self, words):
        word_counts = {}
        for word in words:
            word_counts[word] = word_counts.get(word, 0.0) + 1.0
        return word_counts
    
    def fit(self, X, Y):
        self.num_messages = {}
        self.log_class_priors = {}
        self.word_counts = {}
        self.vocab = set()
     
        n = len(X)
        # Getting the count of number of spam emails
        self.num_messages['spam'] = sum(1 for label in Y if label == 1)
        # Getting the count of number of ham emails
        self.num_messages['ham'] = sum(1 for label in Y if label == 0)
        # Getting the log of spam count
        self.log_class_priors['spam'] = math.log(self.num_messages['spam'] / n)
        # Getting the log of ham count
        self.log_class_priors['ham'] = math.log(self.num_messages['ham'] / n)
        self.word_counts['spam'] = {}
        self.word_counts['ham'] = {}
        
        for x, y in zip(X, Y):
            # Getting the class to which the X belong spam or ham
            c = 'spam' if y == 1 else 'ham'
            counts = self.get_word_counts(self.tokenize(x))
            for word, count in counts.items():
                if word not in self.vocab:
                    # Adding the word to vocab
                    self.vocab.add(word)
                # word_counts ois a dictionary of having two keys spam, ham
                # we take the whatever word we get and add that partcular word in the dictionary under spam or ham key
                # if the word is new a word we initilise it with zero and the increment is performed out of the if the statement
                if word not in self.word_counts[c]:
                    self.word_counts[c][word] = 0.0
     
                # Incrementing word count under spam or ham key
                self.word_counts[c][word] += count
                
    def predict(self, X):
        result = []
        for x in X:
            # Getting the word count of the on whihc the prediction has to be done
            counts = self.get_word_counts(self.tokenize(x))
            spam_score = 0
            ham_score = 0
            for word, _ in counts.items():
                if word not in self.vocab: continue
                
                # add Laplace smoothing
                # Adding the laplace smoothing this done to handle the word whose probabilty is zero
                log_w_given_spam = math.log( (self.word_counts['spam'].get(word, 0.0) + 1) / (self.num_messages['spam'] + len(self.vocab)) )
                log_w_given_ham = math.log( (self.word_counts['ham'].get(word, 0.0) + 1) / (self.num_messages['ham'] + len(self.vocab)) )

                
                # # Question 4 : If we dont take the log the accuracy drops 0
                # log_w_given_spam = math.log( (self.word_counts['spam'].get(word, 0.0) + 1) / (self.num_messages['spam'] + len(self.vocab)) )
                # log_w_given_ham = math.log( (self.word_counts['ham'].get(word, 0.0) + 1) / (self.num_messages['ham'] + len(self.vocab)) )        


                # Question 7 Add 2 : Laplace smoothing    
                # log_w_given_spam = math.log( (self.word_counts['spam'].get(word, 0.0) + 2) / (self.num_messages['spam'] + len(self.vocab)) )
                # log_w_given_ham = math.log( (self.word_counts['ham'].get(word, 0.0) + 2) / (self.num_messages['ham'] + len(self.vocab)) )

                # Question 7 Add 3 : Laplace smoothing    
                # log_w_given_spam = math.log( (self.word_counts['spam'].get(word, 0.0) + 3) / (self.num_messages['spam'] + len(self.vocab)) )
                # log_w_given_ham = math.log( (self.word_counts['ham'].get(word, 0.0) + 3) / (self.num_messages['ham'] + len(self.vocab)) )

     
                # Getting the score of the word in each spam and ham category for individual record
                spam_score += log_w_given_spam
                ham_score += log_w_given_ham
            
            # Adding the scores
            spam_score += self.log_class_priors['spam']
            ham_score += self.log_class_priors['ham']
        
            # If the spam_score is higher than the ham_score the we append 1 else 0
            if spam_score > ham_score:
                result.append(1)
            else:
                result.append(0)
        return result

if __name__ == '__main__':
    X, y = get_data(DATA_DIR)
    MNB = SpamDetector()
    # MNB.fit(X[100:], y[100:])
 
    # pred = MNB.predict(X[:100])
    # true = y[:100]

    # Question 7 : Passing 3 enron folder for training and testing the last 3 enron
    # MNB.fit(X[:17075], y[:17075])
 
    # pred = MNB.predict(X[17075:32386])
    # true = y[17075:32386]
 
    # Checking the accuracy by taking the predicted value and true value 
    accuracy = sum(1 for i in range(len(predict)) if predict[i] == true[i]) / float(len(predict))
    print("{0:.4f}".format(accuracy))

