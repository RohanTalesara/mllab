import math
import matplotlib.pyplot as plt
from smiley import all_points as data

def dbscan(db, dist, eps, minpts):
	c=0
	label = {}

	for p in db:
		if p in label:
			continue

		n = RangeQuery(db, dist, p ,eps)
		if len(n) < minpts:
			label[p] = 'Noise'
			continue

		c=c+1
		label[p] = c

		s= n.copy()
		s.remove(p)

		for q in s:
			if q not in label:
				continue
			if label[q]=='Noise':
				label[q] = c

			label[q] = c

			q_n = RangeQuery(db, dist, q, eps)
			if len(q_n) > minpts:
				s.extend(n)
				s= list(set(s))
	return label

def RangeQuery(db, dist, q, eps):
	n = []

	for p in db:
		if dist(q,p) < eps:
			n.extend([p])

	return n

def Euclid_dist(x,y):
	return math.sqrt(sum([(i-j)**2 for (i,j) in zip(x,y)]))

#data = [(0,0), (1,1), (2,2), (3,3)]
#data = get_smiley_face()

minp = 4
eps = 7
plt.scatter([i[0] for i in data], [i[1] for i in data])
plt.show()

label = dbscan(data, Euclid_dist, eps, minp)

for cluster_num in list(set(label.values())):
	points = [[],[]]

	for x in label:
		if label[x]==cluster_num:
			points[0].append(x[0])
			points[1].append(x[1])

	plt.scatter(points[0], points[1])

plt.show()