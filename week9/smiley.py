from math import cos, sin, pi
import random


def circle(center,radius, n = 100):

	inc = 2*pi/n

	return [(center[0]+radius*cos(theta), center[1]+radius*sin(theta)) for theta in [inc*i for i in range(n)]]

def semi_circle(center,radius, n = 100):

	inc = pi/n

	return [(center[0]+radius*cos(theta),center[1]+ radius*sin(theta)) for theta in [pi+inc*i for i in range(n)]]

def rand_in_circle(center,radius, n = 20):
	radius = radius/2

	inc = pi/n

	return [(center[0]+radius*cos(theta),center[1]+ radius*sin(theta)) for theta in [pi+inc*i for i in range(n)]]



all_points = []

all_points.extend(circle((0,0),10))
all_points.extend(circle((3,3),2,200))
all_points.extend(circle((-3,3),2))
all_points.extend(semi_circle((0,-1),4))